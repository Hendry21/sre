#!/bin/bash

# Update the system
apt-get update && apt-get upgrade -y

# Disable password authentication and enable public key authentication for SSH
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/' /etc/ssh/sshd_config
systemctl restart sshd

# Configure firewall to allow only ports 22 and 80
ufw default deny incoming
ufw default allow outgoing
ufw allow 22/tcp
ufw allow 80/tcp
ufw enable

# Create a non-root user for the application
adduser appuser
usermod -aG sudo appuser

# Disable unnecessary services
systemctl disable apache2

# Remove unused packages
apt-get autoremove -y

# Configure secure DNS resolver
echo "nameserver 1.1.1.1" > /etc/resolv.conf

# Install and configure SSL certificate for HTTPS
apt-get install -y certbot python3-certbot-nginx
certbot --nginx -d example.com

# Enable strong password policy
apt-get install -y libpam-pwquality
sed -i 's/# minlen = 9/minlen = 12/' /etc/security/pwquality.conf
sed -i 's/# dcredit = 0/dcredit = -1/' /etc/security/pwquality.conf
sed -i 's/# ucredit = 0/ucredit = -1/' /etc/security/pwquality.conf
sed -i 's/# ocredit = 0/ocredit = -1/' /etc/security/pwquality.conf
sed -i 's/# lcredit = 0/lcredit = -1/' /etc/security/pwquality.conf
sed -i 's/pam_unix.so/p
