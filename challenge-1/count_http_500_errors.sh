#!/bin/bash

if [ $# -eq 0 ]; then
    echo "Usage: $0 directory_path"
    exit 1
fi

dir=$1

for file in $dir/*; do
    if [ -f "$file" ]; then
        count=$(grep 'HTTP/1.1" 500' "$file" | awk -v date="$(date -d '-10 min' +[%d/%b/%Y:%H:%M:%S)" '$4>date {print}' | wc -l)
        echo "There were $count HTTP 500 errors in $file in the last 10 minutes."
    fi
done
